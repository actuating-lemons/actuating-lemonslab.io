ADDONS=-f scss
OUTPUT_DIR=build

site:
	lektor build $(ADDONS) -O $(OUTPUT_DIR)
	# Lektor does not respect _redirects
	# https://github.com/lektor/lektor/issues/423
	cp assets/_redirects $(OUTPUT_DIR)/_redirects

run_server:
	lektor server $(ADDONS)
