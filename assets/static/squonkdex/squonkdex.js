'use strict';

/**
 * Written similarly to the Squonkdex.
 */
let app = document.app || {
  elements: {
    tabs: {
      index: null,     // The list of all characters.
      charInfo: null,  // Tab for the character info.
      about: null,     // Holds information about the app.
      credits: null,   // Holds the credits & copyright.
    },

    // The buttons for each tab.
    tabButtons: {
      list: null,
      character: null,
      about: null,
      credits: null,
    },

    characterList: null,  // Holds the list of all characters.

    characterInfo: null,   // Holds the character info. Name & Desc are based on
                           // child order.
    characterTable: null,  // The character table

    searchQuery: null,  // The search query area.
  },

  // '' = index
  // 'character:characterId'
  // 'sort:criterion' = index, sorted
  // 'search:name' = index, filtered
  // 'about' = about, 'credits' = credits
  currentTab: null,

  // List of characters from the squonkdex
  characters: null,

  // If true, we know the app is on a mobile device.
  // This allows us to better suit some of the decisions.
  isMobile: false,

  init: function() {
    // Grab references to all the elements;
    this.elements.tabs.index = document.getElementById('tab-list');
    this.elements.characterList = document.getElementById('character-list');

    this.elements.tabs.charInfo = document.getElementById('tab-character');
    this.elements.characterInfo = document.getElementById('character-info');
    this.elements.characterTable = document.getElementById('character-table');

    this.elements.tabs.about = document.getElementById('tab-about');
    this.elements.tabs.credits = document.getElementById('tab-credits');

    this.elements.tabButtons.list = document.getElementById('btn-tab-list');
    this.elements.tabButtons.character =
        document.getElementById('btn-tab-character');
    this.elements.tabButtons.about = document.getElementById('btn-tab-about');
    this.elements.tabButtons.credits =
        document.getElementById('btn-tab-credits');

    this.elements.searchQuery = document.getElementById('search-query');

    this.elements.searchQuery.addEventListener('keydown', (e) => {
      if (e.keyCode === 13) document.app.search();
    });

    // If we're mobile, mark the body for styling reasons.
    this.isMobile =
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
            navigator.userAgent);
    if (this.isMobile) document.body.classList.add('mobile');

    // Load the character sheet
    fetch('/static/squonkdex/characters.json')
        .then((response) => {
          if (!response.ok) {
            alert('Loading characters failed with HTTP ' + response.status);
            throw new Error('Load character fail (HTTP ' + response.status);
          }
          return response;
        })
        .then((response) => response.json())
        .then(data => {
          document.app.characters = data;
          console.log(document.app.characters);
          document.app.swapTab(window.location.hash.slice(1));

          // Let's update the about tab's contents.
          // We need to count all non-secret characters.
          let characterCount = 0;
          Object.values(this.characters).forEach((character) => {
            if (!character.secret) characterCount++;
          });
          document.getElementById('completion-count').innerText =
              characterCount;
          document.getElementById('completion-percent').innerText =
              (characterCount / 80 * 100).toFixed(2);
        });


    // Connect the handlers.
    window.addEventListener('hashchange', (newhash) => {
      let target = window.location.hash.slice(1);  // slices off hash
      // Try swapping tab
      document.app.swapTab(target);
    });
  },

  /**
   * Gets the full name of the character ID.
   * @param {String} charID
   */
  getFullName: function(charID) {
    if (!(charID) in this.characters) return 'Unknown';

    return (typeof this.characters[charID].name === 'string') ?
        this.characters[charID].name :
        this.characters[charID].name.join(' ');
  },

  /**
   * @param {String} newTab
   */
  swapTab: function(newTab) {
    // If characters aren't loaded, don't do anything!
    if (this.characters === null) return;

    // We're already here, move along.
    if (this.currentTab === newTab) return;

    // Swap out the hash.
    window.location.hash = newTab;

    // Hide tabs
    for (let id in this.elements.tabs)
      this.elements.tabs[id].classList.remove('shown');
    for (let id in this.elements.tabButtons)
      this.elements.tabButtons[id].classList.remove('active');

    // Let's figure out our tab.
    // Firstly, any tab that requires us to show the index tab;
    if (newTab.length < 1 || newTab === 'index' ||
        newTab.startsWith('search:') || newTab.startsWith('sort:')) {
      this.elements.tabs.index.classList.add('shown');
      this.elements.tabButtons.list.classList.add('active');

      // clear out old characters
      // TODO: it's probably a waste to regenerate each time if un-needed.
      this.elements.characterList.innerHTML = '';

      // https://stackoverflow.com/a/56553569
      document.getElementsByTagName('meta').namedItem('description').content =
          'The Squonkdex, your comprehensive guide to Squonker characters!';

      document.title = document.getElementsByTagName('meta')
                           .namedItem('description')
                           .content = 'Squonkdex';

      let charIDs = Object.keys(this.characters);
      // Sorting
      if (newTab.startsWith('sort:')) {
        let query = newTab.slice(5);  // chop off sort:

        // helper for age sorting
        let intOrInf = (a) => parseInt(a) === NaN ? Infinity : parseInt(a);

        switch (query) {
          // We want to sort characters by their alphabetic name.
          case 'alphabetic':
          case 'alpha':
          case 'abc':
            charIDs.sort(
                (a, b) =>
                    this.getFullName(a).localeCompare(this.getFullName(b)));
            break;
          // We want to sort it by who has the longest description
          case 'desc':
          case 'description':
            charIDs.sort(
                (a, b) => this.characters[b].description.length -
                    this.characters[a].description.length);
            break;
          // Remove any whose gender is an integer below 2
          // any character whose gender is different than the above condition is
          // queer through being nonbinary.
          case 'queer':
            charIDs = charIDs.filter(
                (id) => (typeof this.characters[id].gender === 'string') ||
                    this.characters[id].gender == 2);
            break;
          // Remove any character who does not specify a custom set of pronouns
          // ofcourse you have blue hair and pronouns
          case 'bluehair':
            charIDs = charIDs.filter(
                (id) => typeof this.characters[id].pronouns === 'string');
            break;
          // Sort by the oldest character first.
          // String ages get bubbled to the bottom.
          case 'old':
            charIDs.sort((a, b) => {
              const ageA = intOrInf(this.characters[a].age);
              const ageB = intOrInf(this.characters[b].age);

              if (!Number.isFinite(ageA) && !Number.isFinite(ageB))
                return 0;  // Both are non-integer values, maintain their order
              else if (!Number.isFinite(ageA))
                return 1;  // ageA is non-integer, move it to the bottom
              else if (!Number.isFinite(ageB))
                return -1;  // ageB is non-integer, move it to the bottom
              else
                return ageB - ageA;  // Both are integers, sort numerically
            });
            break;
          // Sort by the youngest character first.
          // String ages get bubbled to the bottom.
          case 'young':
            charIDs.sort((a, b) => {
              const ageA = intOrInf(this.characters[a].age);
              const ageB = intOrInf(this.characters[b].age);

              if (!Number.isFinite(ageA) && !Number.isFinite(ageB))
                return 0;  // Both are non-integer values, maintain their order
              else if (!Number.isFinite(ageA))
                return 1;  // ageA is non-integer, move it to the bottom
              else if (!Number.isFinite(ageB))
                return -1;  // ageB is non-integer, move it to the bottom
              else
                return ageA - ageB;  // Both are integers, sort numerically
            });
            break;
          default:
            // Switch back to index, invalid query.
            window.location.hash = '';
        }
      }
      // We want to search.
      else if (newTab.startsWith('search:')) {
        let query = newTab.slice(7).toLowerCase();  // Chop off search:

        if (query.length)
          charIDs = charIDs.filter(
              (id) => this.getFullName(id).toLowerCase().includes(query));
        else  // If we don't have a query, remove search:
          window.location.hash = '';
      }

      charIDs.forEach((charID) => {
        // Can't list secret characters!
        if (this.characters[charID].secret) return;

        let fullName = this.getFullName(charID);
        if (this.isMobile) fullName = fullName.split(' ')[0];

        let characterElement = document.createElement('li');
        characterElement.innerHTML = `
<a href="#character:${charID}">
<img src="/static/squonkdex/images/${charID}.png" loading="lazy" alt="">
<p>${fullName}</p>
</a>`;
        this.elements.characterList.appendChild(characterElement);
      });
    } else if (newTab.startsWith('character:')) {
      let charID = newTab.slice(10);  // Chop off character:

      // if the character doesn't exist, just go back to the index.
      if (!(charID in this.characters)) {
        this.swapTab('');
        return;
      }

      this.elements.tabs.charInfo.classList.add('shown');
      this.elements.tabButtons.character.classList.add('active');

      // Reset Scroll
      this.elements.tabs.charInfo.scrollTop = 0;

      // Update where the character button points.
      this.elements.tabButtons.character.href = '#' + newTab;

      let character = this.characters[charID];

      // 0 = image
      // 1 = name
      // 2 = desc
      this.elements.characterInfo.children[0].src =
          `/static/squonkdex/images/${charID}.png`;

      this.elements.characterInfo.children[1].innerText =
          (typeof character.name === 'string') ?
          [character.title, character.name].join(' ') :
          [
            character.title || character.name[0],
            character.name.slice(1).join(' ')
          ].join(' ');

      // https://stackoverflow.com/a/56553569
      document.getElementsByTagName('meta').namedItem('description').content =
          'One of Squonker\'s cool characters!';

      document.title = document.getElementsByTagName('meta')
                           .namedItem('description')
                           .content =
          this.elements.characterInfo.children[1].innerText + ' - Squonkdex';

      // Some character descriptions intentionally break readability for comedy,
      // but this sucks for screenreaders. so the description box is split in
      // twain; visible content and screen reader content.
      this.elements.characterInfo.children[2].children[0].innerText =
          character.description;
      this.elements.characterInfo.children[2].children[1].innerText =
          'screenreader' in character ? character.screenreader :
                                        character.description;

      // Now the table
      // Which I assure you, that DESPITE looking like
      // sphagetti is much nicer than the old innerHTML, or
      // a bunch of ID-ed elements.

      // Name
      this.elements.characterTable.tBodies[0].rows[0].children[1].innerText =
          this.getFullName(charID);

      // Species
      this.elements.characterTable.tBodies[0].rows[1].children[1].innerText =
          character.species;

      // Gender
      let genderSymbol = (typeof (character.gender) === 'string') ?
          'gender-neutrois' :
          ['gender-male', 'gender-female', 'gender-neutrois'][character.gender];

      this.elements.characterTable.tBodies[0]
          .rows[2]
          .children[0]
          .children[0]
          .style = `background-image: url("/img/icons/${genderSymbol}.svg");`;

      this.elements.characterTable.tBodies[0].rows[2].children[1].innerText =
          (typeof character.gender === 'string') ?
          character.gender :
          ['Male', 'Female', 'Non-Binary'][character.gender];

      // Pronouns
      this.elements.characterTable.tBodies[0].rows[3].children[1].innerText =
          (typeof character.pronouns === 'string') ?
          character.pronouns :
          (typeof character.gender === 'string') ?
          'They/Them' :
          ['He/Him', 'She/Her', 'They/Them'][character.gender];

      // Age
      this.elements.characterTable.tBodies[0].rows[4].children[1].innerText =
          character.age;
    }

    // Then the two boring tabs, about & credits
    else if (newTab === 'about') {
      // https://stackoverflow.com/a/56553569
      document.getElementsByTagName('meta').namedItem('description').content =
          'About the Squonkdex!';

      document.title = document.getElementsByTagName('meta')
                           .namedItem('description')
                           .content = 'Squonkdex - About';

      this.elements.tabs.about.classList.add('shown');
      this.elements.tabButtons.about.classList.add('active');
    } else if (newTab === 'credits') {
      // https://stackoverflow.com/a/56553569
      document.getElementsByTagName('meta').namedItem('description').content =
          'Licensing information and credits!';

      document.title = document.getElementsByTagName('meta')
                           .namedItem('description')
                           .content = 'Squonkdex - Credits';

      this.elements.tabs.credits.classList.add('shown');
      this.elements.tabButtons.credits.classList.add('active');
    } else {
      // error, let's go back!
      this.swapTab('');
      return;
    }
  },

  /**
   * Performs a search
   */
  search: function() {
    let query = this.elements.searchQuery.value;
    if (this.isMobile) document.activeElement.blur();  // Hide keyboard.
    this.swapTab('search:' + query);
  }
};
document.app = app;
document.app.init();