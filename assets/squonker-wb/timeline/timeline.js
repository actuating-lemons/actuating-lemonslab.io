'use strict';

const MONTH_NAMES = [
  "Unember",
  "Bidory",
  "Terthi",
  "Quapril",
  "May",
  "Sextemper",
  "September",
  "October",
  "Novaem",
  "December",
  "Undecember",
  "Duodecurary",
  "Tridethi",
];

let app = document.app || {
  // The current timeline
  timeline: {},

  // Timeline list element
  timelineNode: null,

  init: function() {
    this.timelineNode = document.getElementById('timeline');

    // Fetch timeline
    fetch('/squonker-wb/timeline/time.csv')
        .then((response) => {
          if (!response.ok) {
            alert('Loading timeline failed with HTTP ' + response.status);
            throw new Error('Load timeline fail (HTTP ' + response.status);
          }
          return response;
        })
        .then((response) => response.text())
        .then(data => {
          // Parsin's real easy because it's CSV.
          data.split('\n').forEach((line) => {
            // The data SHOULD be time, label
            let row = line.split('\t');
            document.app.timeline[row[0]] = row[1];
          });
          console.log(document.app.timeline);
          document.app.populate();
        });
  },

  populate: function() {
    // 0 is treated as a null value. (there is no year 0)
    let lastYear = 0, lastMonth = 0;

    Object.keys(this.timeline).forEach((time) => {
      let elem = document.createElement('li');
      
      let year = parseInt(time.split(' ')[0]), month = parseInt(time.split(' ')[1])-1;
      
      if (year < 0) {
        elem.classList.add('BCE');
      } else {
        elem.classList.add('CE');
      }

      if (lastYear != 0) {
        // I want distances to feel nice.
        // Large distances are penalised HEAVILY. Small distances should be
        // kept.
        let additionalDistance = Math.abs((lastYear + lastMonth/13) - (year + month/13));

        // Quell large distances
        additionalDistance = Math.abs(Math.log2(additionalDistance));
        // It hasn't happened (yet) but avoid infinity
        if (!isFinite(additionalDistance))
          additionalDistance = 5;

        elem.style.marginTop = `${additionalDistance}rem`;
      }

      if (lastYear != year) {
        // we want to mark the ID so this can be used as a jump to the year!
        elem.id = year;
      }

      lastYear = year, lastMonth = month;

      // If we're beyond 9999, typically we treat it like a number and not a
      // date. So the year can be 9432, but then the year 10,000.
      let friendlyYear = year;
      if (Math.abs(friendlyYear) > 9999) {
        friendlyYear = friendlyYear.toLocaleString();
      }

      elem.innerHTML = `<p>${this.timeline[time]}</p><p class="timeline-time">${MONTH_NAMES[month]}, ${friendlyYear} ${year > 0 ? 'CE' : 'BCE'}</p>`;

      this.timelineNode.appendChild(elem);
    });
  }
};
document.app = app;
document.app.init();