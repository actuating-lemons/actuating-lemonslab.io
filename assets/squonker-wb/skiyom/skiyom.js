'use strict';

/**
 * Written similarly to the Squonkdex.
 */
let app = document.app || {
  elements: {
    tabs: {
      index: null,    // The index tab, which contains the list of content.
      content: null,  // Tab for all the content stuff.
    },
    contentCommentBox: null,  // The container for comments.
    postList: null,           // The index' post list.

    postName:
        null,  // The name of the currently viewed post in the content tab.
    postDesc:
        null,  // the current description of the viewed post in the content tab.
    postImg: null,  // The image of the viewed post.
  },

  // Constant holding month names.
  months: [
    "Unember",
    "Bidory",
    "Terthi",
    "Quapril",
    "May",
    "Sextemper",
    "September",
    "October",
    "Novaem",
    "December",
    "Undecember",
    "Duodecurary",
    "Tridethi",
  ],

  // '' = index
  // 'index' = index
  // 'post:postId'
  currentTab: null,

  // List of characters from the squonkdex
  characters: null,

  // List of all the posts
  posts: null,

  init: function() {
    // Grab references to all the elements;

    this.elements.tabs.index = document.getElementById('tab-index');
    this.elements.postList = document.getElementById('featured-list');

    this.elements.tabs.content = document.getElementById('tab-content');
    this.elements.postName = document.getElementById('post-title');
    this.elements.postDesc = document.getElementById('post-desc');
    this.elements.postImg = document.getElementById('post-img');
    this.elements.contentCommentBox = document.getElementById('comments');

    // Load the character sheet & posts sheet.
    Promise
        .all([
          '/static/squonkdex/characters.json', '/squonker-wb/skiyom/posts.json'
        ].map(u => fetch(u)))
        .then((responses) => Promise.all(responses.map((r) => r.json())))
        .then((data) => {
          document.app.characters = data[0];
          document.app.posts = data[1];
          document.app.swapTab(window.location.hash.slice(1));
        });


    // Connect the handlers.
    window.addEventListener('hashchange', (newhash) => {
      let target = window.location.hash.slice(1);  // slices off hash
      // Try swapping tab
      document.app.swapTab(target);
    });
  },

  /**
   * Takes the time as [HOUR, MINUTE, DAY, MONTH] and returns a properly
   * formatted 12-hour clock.
   * @param {*} time
   */
  createTimestamp: function(time) {
    let hour = time[0] % 12 || 12;
    if (hour < 10) hour = '0' + hour.toString();
    let minute = time[1];
    if (minute < 10) minute = '0' + minute.toString();
    let month = this.months[time[2] - 1];
    let day = time[3];
    return `${hour}:${minute}${time[0] < 12 ? 'am' : 'pm'} ${month},
	${day} 2008`
  },

  /**
   * @param {String} newTab
   */
  swapTab: function(newTab) {
    // If posts aren't loaded, don't do anything!
    if (this.posts === null) return;

    // We're already here, move along.
    if (this.currentTab === newTab) return;

    // Swap out the hash.
    window.location.hash = newTab;

    // Hide tabs
    for (let id in this.elements.tabs)
      this.elements.tabs[id].classList.remove('shown');

    // Let's figure out our tab.
    if (newTab.length < 1 || newTab === 'index') {
      this.elements.tabs.index.classList.add('shown');
      this.elements.postList.innerText = '';


      // Emplace the posts.
      // TODO: It's probably a bit wasteful to reload it every time.
      Object.keys(this.posts).forEach((postId) => {
        let post = this.posts[postId];
        console.log(post);

        let postElement = document.createElement('li');
        postElement.classList.add('media-item');

        postElement.innerHTML = `
<li class="media-item">
	<img src="/squonker-wb/skiyom/postimg/${postId}.png">
	<div class="info">
		<a href="#post:${postId}">${post.name}</a>
		<span class="time">(Featured ${post.time})</span>
		<span class="description">${post.description} <a href="#
		post:${postId}">view</a></span>
	</div>
</li>`;

        this.elements.postList.appendChild(postElement);
      });
    }

    // otherwise let's see if it's a post.
    else if (newTab.startsWith('post:')) {
      let postId = newTab.slice(5);  // Chop off post:

      // if the post doesn't exist, just go back to the index.
      if (!(postId in this.posts)) {
        swapTab('');
        return;
      }

      // Otherwise, prepare it
      this.elements.tabs.content.classList.add('shown');
      let post = this.posts[postId];

      this.elements.postImg.src = `/squonker-wb/skiyom/postimg/${postId}.png`;
      this.elements.postName.innerText = post.name;
      this.elements.postDesc.innerText = post.description;

      // Handle comments now.
      this.elements.contentCommentBox.innerText =
          'comments' in post ? '' : 'No Comments. Consider adding one!';
      if ('comments' in post) {
        post.comments.forEach((comment) => {
          let commentElement = document.createElement('li');
          commentElement.classList.add('comment');
          commentElement.innerHTML = `
<img src="/static/squonkdex/images/${comment.id}.png">
<div>
	<span class="info"><a href="/static/squonkdex#character:${comment.id}">
	${
              this.characters[comment.id].onlinealias ||
              this.characters[comment.id].name.join('.').toLowerCase()}
	</a> | Posted at ${this.createTimestamp(comment.time)}.</span>
	<p>${comment.text}</p>
</div>
`;
          // if there's replies, render them too.
          if ('replies' in comment) {
            let replyHeader = document.createElement('h3');
            replyHeader.innerText = 'Replies';
            commentElement.children[1].appendChild(replyHeader);

            let repliesHolder = document.createElement('ul');
            repliesHolder.classList.add('replies');

            comment.replies.forEach((reply) => {
              let replyElement = document.createElement('li');
              replyElement.classList.add('comment');
              replyElement.innerHTML = `
<span class="info"><a href="/static/squonkdex#character:${reply.id}">
${
                  this.characters[reply.id].onlinealias ||
                  this.characters[reply.id].name.join('.').toLowerCase()}
</a> | Posted at ${this.createTimestamp(reply.time)}.</span>
<p>${reply.text}</p>
`;
              repliesHolder.appendChild(replyElement);
            });

            commentElement.children[1].appendChild(repliesHolder);
          }

          this.elements.contentCommentBox.appendChild(commentElement);
        });
      }
    } else {
      // error, let's go back!
      this.swapTab('');
      return;
    }
  }
};
document.app = app;
document.app.init();